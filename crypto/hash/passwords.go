package hash

import (
	"golang.org/x/crypto/bcrypt"
	"encoding/base64"
)

func HashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 4)
	return base64.StdEncoding.EncodeToString(bytes), err
}

func CheckPasswordHash(password, b64hash string) (bool, error) {
	bytes, err := base64.StdEncoding.DecodeString(b64hash)
	if err != nil {
		return false, err
	}
	err = bcrypt.CompareHashAndPassword(bytes, []byte(password))
	if err != nil {
		return false, err
	}
	return true, nil
}

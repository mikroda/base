package sql

import (
	"github.com/rubenv/sql-migrate"
)

//Generic migrations configuration
type MigrationsConfig struct {
	Database string
	Path     string
}

type MigrationRequest struct {
	Provider Provider

	Direction migrate.MigrationDirection

	Source migrate.MigrationSource
}

// Runs embedded migrations
func RunMigrations(migrationRequest *MigrationRequest) (int, error) {

	provider := migrationRequest.Provider
	db := provider.Db()
	// TODO: migration property to connect to default db and create a new one
	//_, err := db.Exec(fmt.Sprintf("CREATE DATABASE %s;", provider.Config().Database), )
	//if err != nil {
	//	fmt.Println(err)
	//	return 0, err
	//}
	migrationsCount, err := migrate.Exec(db, provider.Dialect(), migrationRequest.Source, migrationRequest.Direction)
	if err != nil {
		return 0, err
	}
	return migrationsCount, nil
}

package postgresql

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
	"github.com/pkg/errors"
	sql2 "gitlab.com/mikroda/base/sql"
)

// PostgreSQL provider
type Provider struct {
	openedDb       *sql.DB
	databaseConfig *sql2.DatabaseConfig
}

// Returns opened connection to database
func (p *Provider) Db() *sql.DB {
	return p.openedDb
}

// Returns dialect
func (p *Provider) Dialect() string {
	return "postgres"
}

// Returns configuration
func (p *Provider) Config() *sql2.DatabaseConfig {
	return p.databaseConfig
}

// Closes the connection if opened
func (p *Provider) Close() error {
	if p.openedDb == nil {
		return errors.New("cannot close, database not opened")
	}
	p.openedDb.Close()
	return nil
}

// Creates a new PostgreSQL provider
func NewProvider(databaseConfig *sql2.DatabaseConfig) (*Provider, error) {
	dbInstance, err := sql.Open("postgres", connectionString(databaseConfig))
	if err != nil {
		return nil, errors.Wrap(err, "unable to open database")
	}

	return &Provider{dbInstance, databaseConfig}, nil
}

func connectionString(dbCfg *sql2.DatabaseConfig) string {
	return fmt.Sprintf("user=%s password=%s dbname=%s host=%s port=%d sslmode=disable",
		dbCfg.Username, dbCfg.Password, dbCfg.Database, dbCfg.Host, dbCfg.Port)
}

package postgresql

import (
	"errors"
	"fmt"
	"github.com/rubenv/sql-migrate"
	sql2 "gitlab.com/mikroda/base/sql"
	"gitlab.com/mikroda/base/testdata/assets"
	"gitlab.com/mikroda/base/testing/docker"
	"os"
	"strconv"
	"testing"
)

var (
	provider sql2.Provider
	config   = sql2.DatabaseConfig{
		Driver:  "postgres",
		Dialect: "postgres",
		Host:    "localhost",
		Port:    5432,

		Database: "postgres",

		Username: "postgres",
		Password: "pass",
	}
)

func TestMain(m *testing.M) {

	pgProvider, pool, resource, err := docker.Postgresql(&config, func() (sql2.Provider, error) {
		return NewProvider(&config)
	})
	if err != nil {
		fmt.Println(err)
	}

	provider = pgProvider

	code := m.Run()

	docker.PurgePostgres(provider, pool, resource)
	os.Exit(code)
}

func TestMigrations(t *testing.T) {

	const testMigrationsDir = "../../testdata/assets/resources/migrations"

	type args struct {
		migrationRequest *sql2.MigrationRequest
	}

	// test both up and down because we would need to drop db otherwise
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Test embedded migrations up",
			args{
				&sql2.MigrationRequest{
					Provider:  provider,
					Direction: migrate.Up,
					Source: migrate.PackrMigrationSource{
						Dir: "migrations",
						Box: assets.Box,
					},
				},
			},
			false,
		},
		{
			"Test embedded migrations down",
			args{
				&sql2.MigrationRequest{
					Provider:  provider,
					Direction: migrate.Down,
					Source: migrate.PackrMigrationSource{
						Dir: "migrations",
						Box: assets.Box,
					},
				},
			},
			false,
		},
		{
			"Test file migrations up",
			args{
				&sql2.MigrationRequest{
					Provider:  provider,
					Direction: migrate.Up,
					Source: migrate.FileMigrationSource{
						Dir: testMigrationsDir,
					},
				},
			},
			false,
		},
		{
			"Test file migrations down",
			args{
				&sql2.MigrationRequest{
					Provider:  provider,
					Direction: migrate.Down,
					Source: migrate.FileMigrationSource{
						Dir: testMigrationsDir,
					},
				},
			},
			false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {

			db := tt.args.migrationRequest.Provider.Db()

			migrationsCount, err := sql2.RunMigrations(tt.args.migrationRequest)
			if (err != nil) != tt.wantErr {
				t.Errorf("RunMigrations() error = %v, wantErr %v", err, tt.wantErr)
			}

			if migrationsCount != 1 {
				t.Errorf("RunMigrations() error = %v, wantErr %v", errors.New("expected 1 migration to run, got "+strconv.Itoa(migrationsCount)), tt.wantErr)
			}

			if (err != nil) != tt.wantErr {
				t.Errorf("RunMigrations() error = %v, wantErr %v", errors.New("unable to start transaction "+strconv.Itoa(migrationsCount)), tt.wantErr)
			}

			rows, err := db.Query(`SELECT EXISTS 
			(
				SELECT 1
				FROM information_schema.tables 
				WHERE table_schema = 'public'
				AND table_name = 'films'
			);`)

			if err != nil {
				t.Errorf("RunMigrations() error = %v, wantErr %v", err, tt.wantErr)
			}
			err = rows.Err()
			if err != nil {
				t.Errorf("RunMigrations() error = %v, wantErr %v", err, tt.wantErr)
			}
			rows.Next()
			var exists bool
			rows.Scan(&exists)

			if !exists && tt.args.migrationRequest.Direction == migrate.Up {
				t.Errorf("RunMigrations() error = %v, wantErr %v", errors.New("expected table to exist"), tt.wantErr)
			} else if exists && tt.args.migrationRequest.Direction == migrate.Down {
				t.Errorf("RunMigrations() error = %v, wantErr %v", errors.New("expected table not to exist"), tt.wantErr)
			}
		})
	}
}

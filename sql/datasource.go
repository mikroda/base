package sql

import (
	"database/sql"
)

//Generic database configuration
type DatabaseConfig struct {
	Driver   string
	Dialect  string

	Host     string
	Port     int32

	Database        string
	DefaultDatabase string

	Username string
	Password string
}

// Database provider interface, holds opened database connection
type Provider interface {
	Db() *sql.DB
	Dialect() string
	Close() error
	Config() *DatabaseConfig
}


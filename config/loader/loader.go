package loader

import (
	"github.com/ghodss/yaml"
	goconfig "github.com/micro/go-micro/config"
	"github.com/micro/go-micro/config/encoder/json"
	"github.com/micro/go-micro/config/source"
	"github.com/micro/go-micro/config/source/env"
	"github.com/micro/go-micro/config/source/file"
	"github.com/micro/go-micro/config/source/memory"
	"github.com/mitchellh/go-homedir"
	"gitlab.com/mikroda/base/io"
	"gitlab.com/mikroda/base/logger"
	"path"
)

var log = logger.Get()

// Configuration for loader that will load YAML configuration with priorities:
// 1. environment variables
// 2. configuration file
// 3. raw memory
type DefaultLoaderConfiguration struct {
	// Prefix (stripped) for environment variable that will be merged
	EnvVarPrefix string
	// Filename of config file to load
	ConfigFilename string
	// Filename of config file to load as fallback relative to home directory eg. '.myservice/config.yaml'
	DefaultConfigHomeFilename string
	// Raw loaded config from memory resource
	RawMemoryConfig []byte
}

// Loads configuration in default order specified by DefaultLoaderConfiguration
func DefaultConfigLoader(loaderCfg *DefaultLoaderConfiguration) (goconfig.Config, error) {
	config := goconfig.NewConfig()
	enc := json.NewEncoder()
	var cfgFileName = loaderCfg.ConfigFilename
	if cfgFileName == "" {
		home, err := homedir.Dir()
		if err != nil {
			return nil, err
		}
		if loaderCfg.DefaultConfigHomeFilename != "" {
			cfgFileName = path.Join(home, loaderCfg.DefaultConfigHomeFilename)
		}
	}

	var sources []source.Source

	if loaderCfg.RawMemoryConfig != nil && len(loaderCfg.RawMemoryConfig) > 0 {
		rawJsonConfig, err := yaml.YAMLToJSON(loaderCfg.RawMemoryConfig)
		if err != nil {
			return nil, err
		}
		// accepts json only
		sources = append(sources, memory.NewSource(
			memory.WithData(rawJsonConfig),
			source.WithEncoder(enc),
		))
	}
	if cfgFileName != "" {
		if io.FileExists(cfgFileName) {
			sources = append(sources, file.NewSource(
				file.WithPath(cfgFileName),
				source.WithEncoder(enc),
			))
		} else {
			log.Warns("Configuration file not found, skipping", "file", cfgFileName)
		}
	}
	if loaderCfg.EnvVarPrefix != "" {
		sources = append(sources, env.NewSource(
			env.WithStrippedPrefix(loaderCfg.EnvVarPrefix),
		))
	}

	err := config.Load(sources...)
	if err != nil {
		return nil, err
	}
	return config, nil
}

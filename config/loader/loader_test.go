package loader

import (
	"errors"
	goconfig "github.com/micro/go-micro/config"
	"gitlab.com/mikroda/base/testdata/assets"
	"os"
	"strings"
	"testing"
)

func TestDefaultConfigLoader(t *testing.T) {

	type args struct {
		loaderCfg *DefaultLoaderConfiguration
	}
	type wanted struct {
		key   string
		value string
	}

	tests := []struct {
		name    string
		args    args
		want    []wanted
		wantErr bool
	}{
		{
			name:    "test configuration loaded and merged properly",
			wantErr: false,
			want: []wanted{
				{key: "memory.data", value: "memory_data"},
				{key: "file.data", value: "file_data"},
				{key: "envvar.data", value: "envvar_data"},
				{key: "shared.memory", value: "memory"}, //original from memory
				{key: "shared.file", value: "file"},     //overridden by file
				{key: "shared.envvar", value: "envvar"}, //overridden by file and then envvar
			},
			args: struct{ loaderCfg *DefaultLoaderConfiguration }{loaderCfg: &DefaultLoaderConfiguration{
				RawMemoryConfig: assets.Box.Bytes("config/config-memory.yaml"),
				ConfigFilename:  "../../testdata/assets/resources/config/config.yaml",
				//DefaultConfigHomeFilename: "", // wont need this
				EnvVarPrefix: "TEST",
			}},
		},
	}

	//set envvar
	err := os.Setenv("TEST_ENVVAR_DATA", "envvar_data")
	if err != nil {
		t.Errorf("Failed to set ennvar")
	}
	err = os.Setenv("TEST_SHARED_ENVVAR", "envvar")
	if err != nil {
		t.Errorf("Failed to set ennvar")
	}
	for _, tt := range tests {
		d := 241412
		d = d
		t.Run(tt.name, func(t *testing.T) {
			got, err := DefaultConfigLoader(tt.args.loaderCfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("DefaultConfigLoader() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			for _, v := range tt.want {
				if !checkKV(t, got, v.key, v.value) {
					return
				}
			}
		})
	}
}

func checkKV(t *testing.T, got config.Config, key, wantedValue string) bool {
	splitKey := strings.Split(key, ".")
	val := got.Get(splitKey...).String("")
	if val != wantedValue {
		t.Errorf("DefaultConfigLoader() error = %v", errors.New("value for '"+key+"' key should be: '"+wantedValue+"'"))
		return false
	}
	return true
}

package docker

import (
	"github.com/hashicorp/consul/api"
	"github.com/ory/dockertest"
	"github.com/ory/dockertest/docker"
	"github.com/pkg/errors"
	"gitlab.com/mikroda/base/logger"
	"gitlab.com/mikroda/base/sql"
	"strconv"
)

var log = logger.Get()

func Postgresql(databaseConfiguration sql.DatabaseConfig, providerFunc func() (sql.Provider, error)) (sql.Provider, *dockertest.Pool, *dockertest.Resource, error) {
	log.Infom("Starting postgres docker image")
	var provider sql.Provider
	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not connect to docker")
	}

	resource, err := pool.Run("postgres", "10", []string{"POSTGRES_PASSWORD=" + databaseConfiguration.Password})
	if err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not start resource")

	}
	if err := pool.Retry(func() error {
		var err error
		i, err := strconv.Atoi(resource.GetPort("5432/tcp"))
		if err != nil {
			log.Errore("Could not connect to database: %s", err)
		}
		// set resolved port from docker image
		databaseConfiguration.Port = int32(i)

		provider, err = providerFunc()

		if err != nil {
			log.Errore("Could not connect to database ", err)
		}

		return provider.Db().Ping()
	}); err != nil {
		return nil, nil, nil, errors.Wrap(err, "could not connect to docker image")
	}
	log.Infom("Postgres docker image started")
	return provider, pool, resource, nil
}

func PurgePostgres(provider sql.Provider, pool *dockertest.Pool, resource *dockertest.Resource) {
	if err := provider.Close(); err != nil {
		log.Errore("Could not close databae connection", err)
	}

	Purge(pool, resource)
}

func Consul() (*dockertest.Pool, *dockertest.Resource, error) {
	log.Infom("Starting consul docker image")
	pool, err := dockertest.NewPool("")
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not connect to docker")
	}
	options := dockertest.RunOptions{
		Repository: "consul",
		Tag:        "1.1.0",
		Cmd:        []string{"agent", "-dev", "-client", "0.0.0.0"},
		Env:        []string{"CONSUL_BIND_INTERFACE=eth0"},
		Entrypoint: []string{"consul"},
		PortBindings: map[docker.Port][]docker.PortBinding{
			"8500/tcp": {{HostIP: "", HostPort: "8500"}},
		},
	}
	resource, err := pool.RunWithOptions(&options)
	if err != nil {
		return nil, nil, errors.Wrap(err, "could not start resource")

	}
	if err := pool.Retry(func() error {
		var err error

		client, err := api.NewClient(api.DefaultConfig())
		if err != nil {
			log.Errore("Could not connect to consul: ", err)
		}
		_, _, err = client.KV().Get("aa", &api.QueryOptions{})
		if err != nil {
			log.Errore("Could not connect to consul: ", err)
		}

		return err
	}); err != nil {
		return nil, nil, errors.Wrap(err, "could not connect to docker image")
	}
	log.Infom("Consul docker image started")
	return pool, resource, nil
}

func Purge(pool *dockertest.Pool, resource *dockertest.Resource) {

	// You can't defer this because os.Exit doesn't care for defer
	if err := pool.Purge(resource); err != nil {
		log.Errore("Could not purge postgresResource", err)
	}
}

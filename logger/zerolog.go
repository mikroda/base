package logger

import (
	"fmt"
	"io"
	"os"
	"runtime"
	"strconv"

	"github.com/rs/zerolog"
	io2 "gitlab.com/mikroda/base/io"
	"reflect"
	"strings"
)

var (
	ConsoleWriter = zerolog.ConsoleWriter{Out: os.Stdout}
	JsonWriter    = zerolog.SyncWriter(os.Stdout)

	// proxy to stdout by default
	defaultWriterProxy = &writerProxy{
		writer: ConsoleWriter,
	}

	// used to skip proper number of lines for easier console debugging
	currentPackage = reflect.TypeOf(writerProxy{}).PkgPath()
)

// Sets output writer
func SetOutput(writer io.Writer) {
	defaultWriterProxy.SetWriter(writer)
}

// Sets the NilWriter as the output,
// useful when no log output is wanted to STDOUT
func Disable() {
	SetOutput(io2.NewNilWriter())
}

// A proxy to the real Writer, can be reconfigured at runtime
type writerProxy struct {
	levelWriter zerolog.LevelWriter
	writer      io.Writer
}

func (w *writerProxy) WriteLevel(level zerolog.Level, bytes []byte) (n int, err error) {
	if w.levelWriter != nil {
		return w.levelWriter.WriteLevel(level, bytes)
	}
	return w.writer.Write(bytes)
}

func (w *writerProxy) Write(bytes []byte) (int, error) {
	return w.writer.Write(bytes)
}

func (w *writerProxy) SetWriter(writer io.Writer) {
	w.writer = writer
	w.levelWriter = w.writer.(zerolog.LevelWriter)
}

// Error hook, so we can track stack track in console
// Note that this dumps the multiline stack trace directly in console as it would be otherwise single line/unreadable
type errHook struct {
}

func (errHook) Run(e *zerolog.Event, level zerolog.Level, message string) {
	if level == zerolog.ErrorLevel {
		_, file, line, _ := runtime.Caller(3)
		if strings.Contains(file, currentPackage) {
			_, file, line, _ = runtime.Caller(4)
		}
		fmt.Println(file + ":" + strconv.FormatInt(int64(line), 10))
	}
}

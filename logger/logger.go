package logger

import (
	"github.com/rs/zerolog"
)

type Logger struct {
	zerolog.Logger
}

// Debug helper for message with message only
func (l *Logger) Debugm(message string) {
	l.Debug().Msg(message)
}

// Info helper for message with message only
func (l *Logger) Infom(message string) {
	l.Info().Msg(message)
}

// Info helper for message with one string key-value
func (l *Logger) Infos(message, key, value string) {
	l.Info().Str(key, value).Msg(message)
}

func (l *Logger) Infoss(message string, key1 string, value1 string, key2 string, value2 string) {
	l.Info().Str(key1, value1).Str(key2, value2).Msg(message)
}

// Warn helper for message with message only
func (l *Logger) Warnm(message string) {
	l.Warn().Msg(message)
}

// Warn helper for message with one string key-value
func (l *Logger) Warns(message, key, value string) {
	l.Warn().Str(key, value).Msg(message)
}

// Error helper for message with message only
func (l *Logger) Errorm(message string) {
	l.Error().Msg(message)
}

// Error helper for message with message and error
func (l *Logger) Errore(message string, err error) {
	l.Error().Err(err).Msg(message)
}

// Error helper for message with one string key-value
func (l *Logger) Errors(message, key, value string, err error) {
	l.Info().Str(key, value).Err(err).Msg(message)
}

func (l *Logger) WithStr(key string, value string) *Logger {
	return &Logger{l.Logger.With().Str(key, value).Logger()}
}

// Returns logger with timestamp and caller information
func Get() *Logger {
	log := zerolog.
		New(defaultWriterProxy).
		With().
		Timestamp().
		Logger()

	if defaultWriterProxy.writer == &ConsoleWriter {
		log = log.Hook(errHook{})
	}
	return &Logger{log}
}

// Returns logger with timestamp and caller information
func GetWith(customizer func(ctx zerolog.Context) zerolog.Logger) *Logger {
	defaultCtx := zerolog.
		New(defaultWriterProxy).
		With()
	log := customizer(defaultCtx)
	if defaultWriterProxy.writer == &ConsoleWriter {
		log = log.Hook(errHook{})
	}
	return &Logger{log}
}

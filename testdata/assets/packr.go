package assets

import (
	"github.com/gobuffalo/packr/v2"
)

var Box *packr.Box

// Loads embedded resources in memory
func init() {
	Box = packr.NewBox("resources")
}

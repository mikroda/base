-- +migrate Up

CREATE TABLE films (
    code        char(5) CONSTRAINT firstkey PRIMARY KEY,
    title       varchar(40) NOT NULL,
    did         integer NOT NULL,
    date_prod   date,
    kind        varchar(10),
    len         interval hour to minute
);
INSERT into films(code, title, did,  kind) values ('test','some title',1,'movie');

-- +migrate Down

DROP TABLE films;
package io

import "os"

// Dummy NO-OP writer
type NilWriter struct {
}

func (NilWriter) Write(p []byte) (n int, err error) {
	//NO-OP
	return len(p), nil
}

func NewNilWriter() *NilWriter {
	return &NilWriter{}
}


func FileExists(path string)bool{
	if _, err := os.Stat(path); os.IsNotExist(err) {
		return false
	}
	return true
}
module gitlab.com/mikroda/base

go 1.13

require (
	github.com/Nvveen/Gotty v0.0.0-20120604004816-cd527374f1e5 // indirect
	github.com/cenkalti/backoff v2.2.1+incompatible // indirect
	github.com/ghodss/yaml v1.0.0
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gobuffalo/packr v1.30.1 // indirect
	github.com/gobuffalo/packr/v2 v2.7.1
	github.com/gotestyourself/gotestyourself v2.2.0+incompatible // indirect
	github.com/hashicorp/consul/api v1.2.0
	github.com/lib/pq v1.2.0
	github.com/mattn/go-sqlite3 v1.11.0 // indirect
	github.com/micro/go-micro v1.13.2
	github.com/mitchellh/go-homedir v1.1.0
	github.com/ory/dockertest v3.3.5+incompatible
	github.com/pkg/errors v0.8.1
	github.com/rs/zerolog v1.15.0
	github.com/rubenv/sql-migrate v0.0.0-20191022111038-5cdff0d8cc42
	github.com/ziutek/mymysql v1.5.4 // indirect
	golang.org/x/crypto v0.0.0-20191011191535-87dc89f01550
	gopkg.in/gorp.v1 v1.6.2-0.20191004195204-65378da88bd2 // indirect
)
